From fbb40cd2fa6b02a30b5f3d5e8f752aacf3812d92 Mon Sep 17 00:00:00 2001
From: Sukrut Bellary <sukrut.bellary@linux.com>
Date: Mon, 25 Mar 2024 09:35:10 -0500
Subject: [PATCH] arm64: dts: ti: k3-am625-beagleplay: Use mmc-pwrseq for
 wl18xx enable

BeaglePlay SBC[1] has Texas Instrument's WL18xx WiFi chipset[2].

Currently, WLAN_EN is configured as regulator and regulator-always-on.
However, the timing and wlan_en sequencing is not correctly modelled.

This causes the sdio access to fail during runtime-pm power operations
saving or during system suspend/resume/hibernation/freeze operations.
This is because the WLAN_EN line is not deasserted to low '0' to power
down the WiFi. So during restore, the WiFi driver tries to load the FW
without following correct power sequence. WLAN_EN => '1'/assert (high)
to power-up the chipset.

Use mmc-pwrseq-simple to drive TI's WiFi (WL18xx) chipset enable
'WLAN_EN'. mmc-pwrseq-simple provides power sequence flexibility with
support for post power-on and power-off delays.

Typical log signature that indicates this bug is:
wl1271_sdio mmc2:0001:2: sdio write failed (-110)

Followed by possibly a kernel warning (depending on firmware present):
WARNING: CPU: 1 PID: 45 at drivers/net/wireless/ti/wlcore/sdio.c:123 wl12xx_sdio_raw_write+0xe4/0x168 [wlcore_sdio]

[1] https://www.beagleboard.org/boards/beagleplay
[2] https://www.ti.com/lit/ds/symlink/wl1807mod.pdf

Fixes: f5a731f0787f ("arm64: dts: ti: Add k3-am625-beagleplay")
Suggested-by: Shengyu Qu <wiagn233@outlook.com>
Signed-off-by: Sukrut Bellary <sukrut.bellary@linux.com>
Signed-off-by: Nishanth Menon <nm@ti.com>
---
 .../arm64/boot/dts/ti/k3-am625-beagleplay.dts | 27 +++++++++----------
 1 file changed, 12 insertions(+), 15 deletions(-)

diff --git a/arch/arm64/boot/dts/ti/k3-am625-beagleplay.dts b/arch/arm64/boot/dts/ti/k3-am625-beagleplay.dts
index a34e0df2ab86..ffc613543968 100644
--- a/arch/arm64/boot/dts/ti/k3-am625-beagleplay.dts
+++ b/arch/arm64/boot/dts/ti/k3-am625-beagleplay.dts
@@ -82,6 +82,17 @@ wkup_r5fss0_core0_dma_memory_region: r5f-dma-memory@9db00000 {
 		};
 	};
 
+	sdio_pwrseq: sdio-pwrseq {
+		compatible = "mmc-pwrseq-simple";
+		pinctrl-names = "default";
+		pinctrl-0 = <&wifi_en_pins_default>;
+		/* Interal power on time(Figure 8-3) * 2 */
+		post-power-on-delay-ms = <10>;
+		/* Re-enable time(Figure 8-2) + 20uS */
+		power-off-delay-us = <80>;
+		reset-gpios = <&main_gpio0 38 GPIO_ACTIVE_LOW>;
+	};
+
 	vsys_5v0: regulator-1 {
 		bootph-all;
 		compatible = "regulator-fixed";
@@ -104,20 +115,6 @@ vdd_3v3: regulator-2 {
 		regulator-boot-on;
 	};
 
-	wlan_en: regulator-3 {
-		/* OUTPUT of SN74AVC2T244DQMR */
-		compatible = "regulator-fixed";
-		regulator-name = "wlan_en";
-		regulator-min-microvolt = <1800000>;
-		regulator-max-microvolt = <1800000>;
-		enable-active-high;
-		regulator-always-on;
-		vin-supply = <&vdd_3v3>;
-		gpio = <&main_gpio0 38 GPIO_ACTIVE_HIGH>;
-		pinctrl-names = "default";
-		pinctrl-0 = <&wifi_en_pins_default>;
-	};
-
 	vdd_3v3_sd: regulator-4 {
 		/* output of TPS22918DBVR-U21 */
 		bootph-all;
@@ -839,13 +836,13 @@ &sdhci1 {
 };
 
 &sdhci2 {
-	vmmc-supply = <&wlan_en>;
 	pinctrl-names = "default";
 	pinctrl-0 = <&wifi_pins_default>, <&wifi_32k_clk>;
 	non-removable;
 	ti,fails-without-test-cd;
 	cap-power-off-card;
 	keep-power-in-suspend;
+	mmc-pwrseq = <&sdio_pwrseq>;
 	assigned-clocks = <&k3_clks 157 158>;
 	assigned-clock-parents = <&k3_clks 157 160>;
 	#address-cells = <1>;
-- 
2.39.2

