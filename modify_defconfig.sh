#!/bin/bash

cd ./linux/

echo "make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- defconfig"
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- defconfig

echo "make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- savedefconfig"
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- savedefconfig

cp -v defconfig compare_defconfig

echo "Config tweaks"
./scripts/config --disable CONFIG_LOCALVERSION_AUTO
./scripts/config --disable CONFIG_DEBUG_INFO
./scripts/config --enable CONFIG_DEBUG_INFO_NONE
./scripts/config --disable CONFIG_DEBUG_INFO_DWARF4
./scripts/config --disable CONFIG_DEBUG_INFO_COMPRESSED_NONE
./scripts/config --disable CONFIG_DEBUG_INFO_SPLIT
./scripts/config --disable DEBUG_INFO_DWARF_TOOLCHAIN_DEFAULT

./scripts/config --enable CONFIG_OF_OVERLAY
./scripts/config --module CONFIG_DRM_POWERVR

./scripts/config --disable CONFIG_MODULE_COMPRESS_ZSTD
./scripts/config --enable CONFIG_MODULE_COMPRESS_XZ
./scripts/config --enable CONFIG_GPIO_AGGREGATOR

#enable iwd
./scripts/config --enable CONFIG_CRYPTO_USER_API_HASH
./scripts/config --enable CONFIG_CRYPTO_USER_API_SKCIPHER
./scripts/config --enable CONFIG_KEY_DH_OPERATIONS
./scripts/config --enable CONFIG_CRYPTO_ECB
./scripts/config --enable CONFIG_CRYPTO_MD4
./scripts/config --enable CONFIG_CRYPTO_MD5
./scripts/config --enable CONFIG_CRYPTO_CBC
./scripts/config --enable CONFIG_CRYPTO_SHA256
./scripts/config --enable CONFIG_CRYPTO_AES
./scripts/config --enable CONFIG_CRYPTO_DES
./scripts/config --enable CONFIG_CRYPTO_CMAC
./scripts/config --enable CONFIG_CRYPTO_HMAC
./scripts/config --enable CONFIG_CRYPTO_SHA51

#enable MIKROBUS
./scripts/config --enable CONFIG_SPI_OMAP24XX
./scripts/config --enable CONFIG_W1
#./scripts/config --enable CONFIG_MIKROBUS

echo "make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- olddefconfig"
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- olddefconfig

cp -v .config ../public/defconfig_pre_savedefconfig

echo "make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- savedefconfig"
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- savedefconfig

cp -v defconfig ../public/defconfig

diff -u compare_defconfig defconfig

rm -rf compare_defconfig defconfig || true

cd ../
